import Form from './Form'
import Table from './Table'

export { Form, Table }

export default {
  Form,
  Table,
}
