/*
 * @Author: linyi
 * @Date: 2019-01-03 14:18:28
 * @Last Modified by: linyi
 * @Last Modified time: 2019-03-15 14:53:37
 */

import React, { Component } from 'react'
import moment from 'moment'
import {
  Form, Button, Input, DatePicker,
} from 'antd'
import '../style/Form.scss'
import { SelectorWithAll } from 'Comps'

class App extends Component {
  constructor() {
    super()
    this.state = {}
  }

  render() {
    const {
      orderStatusChoicesLoading,
      orderStatusChoices,
      form: { getFieldDecorator },
    } = this.props

    return (
      <Form className="common-filter-form" layout="inline">
        {/* <Form.Item label="退货申请号">
          {getFieldDecorator('id')(<Input placeholder="请输入退货申请号" />)}
        </Form.Item> */}
        <Form.Item label="订单号码">
          {getFieldDecorator('orderCode')(<Input placeholder="请输入订单号" />)}
        </Form.Item>
        <Form.Item label="退货申请时间">
          {getFieldDecorator('updateTime', {
            initialValue: [moment().subtract(1, 'week'), moment()],
          })(<DatePicker.RangePicker />)}
        </Form.Item>
        <Form.Item label="订单状态">
          {getFieldDecorator('orderStatus')(
            <SelectorWithAll
              loading={orderStatusChoicesLoading}
              choices={orderStatusChoices}
              placeholder="请选择订单状态"
            />,
          )}
        </Form.Item>
        <Form.Item className="form-btns">
          <Button
            type="primary"
            className="mr10"
            onClick={() => this.props.handleSearchParams({
              ...this.props.form.getFieldsValue(),
              updateTime: null,
              beginTime:
                  (this.props.form.getFieldsValue().updateTime
                    && this.props.form.getFieldsValue().updateTime.length
                    && moment(
                      this.props.form.getFieldsValue().updateTime[0],
                    ).format('YYYY-MM-DD'))
                  || '',
              endTime:
                  (this.props.form.getFieldsValue().updateTime
                    && this.props.form.getFieldsValue().updateTime.length
                    && moment(
                      this.props.form.getFieldsValue().updateTime[1],
                    ).format('YYYY-MM-DD'))
                  || '',
              orderStatus:
                  (this.props.form.getFieldsValue().orderStatus
                    && this.props.form.getFieldsValue().orderStatus.join(','))
                  || '',
            })
            }
          >
            查询
          </Button>
          <Button onClick={() => this.props.form.resetFields()}>重置</Button>
        </Form.Item>
      </Form>
    )
  }
}
export default Form.create()(App)
