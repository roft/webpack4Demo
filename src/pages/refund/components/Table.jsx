import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Table } from 'antd'
import '../style/Table.scss'

export default class App extends Component {
  constructor() {
    super()

    this.columns = [
      {
        title: '序号',
        dataIndex: 'index',
        render: (text, record, index) => `${index + 1}`,
      },
      {
        title: '退货申请号',
        dataIndex: 'id',
        key: 'id',
        render: (text, record) => (
          <a href={`/refunds/${text}/${record.isPartial}`}>{text}</a>
        ),
      },
      { title: '订单号码', dataIndex: 'orderCode', key: 'orderCode' },
      { title: '取消原因', dataIndex: 'reason', key: 'reason' },
      { title: '退货申请时间', dataIndex: 'createdTime', key: 'createdTime' },
      { title: '状态变更时间', dataIndex: 'updatedTime', key: 'updatedTime' },
      {
        title: '状态',
        dataIndex: 'orderStatus',
        key: 'orderStatus',
        width: 150,
        fixed: 'right',
      },
    ]
  }

  /* 上下架 */
  handleDisable1 = record => {
    const { handleDisable } = this.props
    handleDisable(record)
  }

  /* 修改 弹框 */
  handleEdit = record => {
    const { showEditPopUp } = this.props
    showEditPopUp(record)
  }

  /* 删除 */
  handleDelete = record => {
    const { handleDelete } = this.props

    Modal.confirm({
      title: '提示',
      content: '确认删除吗',
      okText: '确认',
      cancelText: '取消',
      onOk: () => handleDelete(record),
    })
  }

  render() {
    const {
      dataSource,
      pageIndex,
      totalPage,
      pageSize,
      handlePageSizeChange,
      tableLoading,
      // selectedRowKeys,
      // handleSelectChange,
      // updateSelectedRows,
    } = this.props
    return (
      <section className="common-result-table-box">
        <div className="crtb-header">搜索结果</div>
        <Table
          columns={this.columns}
          dataSource={dataSource}
          // rowSelection={{
          //   selectedRowKeys, // 表格中选中的数据集下标
          //   onChange: handleSelectChange,
          //   onSelect: updateSelectedRows,
          // }}
          pagination={{
            total: totalPage,
            current: pageIndex,
            pageSize,
            showQuickJumper: true, // 跳转到目标页码
            showSizeChanger: true, // 可修改当前页显示条数
            showTotal: total => `共 ${total} 条`, // 显示总条数
            pageSizeOptions: ['50', '100', '200', '500'], // 具体的显示条数规则
            onShowSizeChange: handlePageSizeChange,
            onChange: handlePageSizeChange,
          }}
          loading={tableLoading}
        />
      </section>
    )
  }
}

App.propTypes = {
  dataSource: PropTypes.array.isRequired,
  pageIndex: PropTypes.number.isRequired,
  totalPage: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  handlePageSizeChange: PropTypes.func.isRequired,
  tableLoading: PropTypes.bool.isRequired,
  // selectedRowKeys: PropTypes.array.isRequired,
  // handleSelectChange: PropTypes.func.isRequired,
  // updateSelectedRows: PropTypes.func.isRequired,
}
