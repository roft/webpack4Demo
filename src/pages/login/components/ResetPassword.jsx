/**
 * Created by linyi on 2019/3/11
 */
import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Form, Icon, Input, Button } from 'antd'

const { Item: FormItem } = Form

const hasErrors = fieldsErr =>
  Object.keys(fieldsErr).some(field => fieldsErr[field])

class ResetPassword extends React.Component {
  state = {
    confirmDirty: false,
    err: ''
  }

  componentDidMount() {
    const {
      form: { validateFields }
    } = this.props
    validateFields()
  }

  onSubmit = e => {
    e.preventDefault()

    const {
      form: { validateFields },
      onSubmit: handleSubmit
    } = this.props
    validateFields((err, values) => {
      if (!err) {
        handleSubmit(values)
      }
    })
  }

  onConfirmBlur = e => {
    const { value } = e.target
    const { confirmDirty } = this.state
    this.setState({ confirmDirty: confirmDirty || !!value })
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props
    if (value && value !== form.getFieldValue('password')) {
      const err = '两次输入的密码不一致'
      this.setState({ err })
      callback(err)
    } else {
      this.setState({ err: '' })
      callback()
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props
    const { confirmDirty } = this.state
    if (value && confirmDirty) {
      form.validateFields(['confirm'], { force: true })
    }
    callback()
  }

  render() {
    const {
      form: { getFieldDecorator, getFieldsError },
      loading,
      errMsg: passedErr,
      onReturnClick
    } = this.props
    const { err } = this.state

    const errMsg = passedErr || err

    return (
      <section className="login--sign-in form-box">
        <header>
          <h1>重置密码</h1>
          <button
            className="btn-text return-btn"
            type="button"
            onClick={onReturnClick}
          >
            返回登录
            <Icon type="rollback" />
          </button>
        </header>
        <Form onSubmit={this.onSubmit} className="login-common-form">
          <FormItem>
            {getFieldDecorator('password', {
              rules: [
                { required: true },
                { validator: this.validateToNextPassword }
              ]
            })(
              <Input
                suffix={<Icon type="lock" />}
                placeholder="6-12位，含数字及大、小写字母"
                type="password"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('confirm', {
              rules: [
                { required: true },
                { validator: this.compareToFirstPassword }
              ]
            })(
              <Input
                suffix={<Icon type="lock" />}
                placeholder="请再次输入新密码"
                type="password"
                onBlur={this.onConfirmBlur}
              />
            )}
          </FormItem>
          <section className="action-box">
            <div className="err-box">
              <p>
                {errMsg && (
                  <Fragment>
                    <Icon type="exclamation-circle" theme="filled" />
                    {errMsg}
                  </Fragment>
                )}
              </p>
            </div>
            <Button
              type="primary"
              htmlType="submit"
              disabled={loading || hasErrors(getFieldsError())}
            >
              确认
            </Button>
          </section>
        </Form>
      </section>
    )
  }
}

ResetPassword.propTypes = {
  loading: PropTypes.bool.isRequired,
  errMsg: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onReturnClick: PropTypes.func.isRequired,
  form: PropTypes.objectOf(PropTypes.func).isRequired
}

export default Form.create()(ResetPassword)
