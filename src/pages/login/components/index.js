/**
 * Created by linyi on 2019/3/11
 */
import SignIn from './SignIn'
import ValidatePhone from './ValidatePhone'
import ResetPassword from './ResetPassword'

export { SignIn, ValidatePhone, ResetPassword }

export default {
  SignIn,
  ValidatePhone,
  ResetPassword
}
