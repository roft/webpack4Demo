/**
 * Created by linyi on 2019/3/11
 */
import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Form, Icon, Input, Button } from 'antd'

const { Item: FormItem } = Form
const hasErrors = fieldsErr =>
  Object.keys(fieldsErr).some(field => fieldsErr[field])

class ValidatePhone extends React.Component {
  componentDidMount() {
    const {
      form: { validateFields }
    } = this.props
    validateFields()
  }

  onSubmit = e => {
    e.preventDefault()

    const {
      form: { validateFields },
      onSubmit: handleSubmit
    } = this.props
    validateFields((err, values) => {
      if (!err) {
        handleSubmit(values)
      }
    })
  }

  onCodeBtnClick = () => {
    const {
      form: { getFieldValue },
      onFetchCodeClick
    } = this.props
    onFetchCodeClick(getFieldValue('mobile'))
  }

  render() {
    const {
      form: { getFieldDecorator, getFieldsError, getFieldValue },
      loading,
      errMsg,
      fetchingCode,
      fetchCodeTimer,
      onReturnClick
    } = this.props

    const mobile = (getFieldValue('mobile') || '').trim()
    const isSMSBtnDisabled =
      fetchingCode || fetchCodeTimer < 60 || !/^1[34578]\d{9}$/.test(mobile)

    return (
      <section className="login--sign-in form-box">
        <header>
          <h1>手机验证码</h1>
          <button
            className="btn-text return-btn"
            type="button"
            onClick={onReturnClick}
          >
            返回登录
            <Icon type="rollback" />
          </button>
        </header>
        <Form onSubmit={this.onSubmit} className="login-common-form">
          <FormItem>
            {getFieldDecorator('mobile', {
              rules: [{ required: true }]
            })(
              <Input
                suffix={<Icon type="mobile" />}
                placeholder="请输入手机号"
              />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('smsCode', {
              rules: [{ required: true }]
            })(
              <Input suffix={<Icon type="key" />} placeholder="请输入验证码" />
            )}
            <Button
              className="btn-plain get-sms-code-btn"
              disabled={isSMSBtnDisabled}
              onClick={this.onCodeBtnClick}
            >
              {fetchCodeTimer < 60 ? `${fetchCodeTimer}s后获取` : '获取验证码'}
            </Button>
          </FormItem>
          <section className="action-box">
            <div className="err-box">
              <p>
                {errMsg && (
                  <Fragment>
                    <Icon type="exclamation-circle" theme="filled" />
                    {errMsg}
                  </Fragment>
                )}
              </p>
            </div>
            <Button
              type="primary"
              htmlType="submit"
              disabled={loading || hasErrors(getFieldsError())}
            >
              下一步
            </Button>
          </section>
        </Form>
      </section>
    )
  }
}

ValidatePhone.propTypes = {
  loading: PropTypes.bool.isRequired,
  errMsg: PropTypes.string.isRequired,
  fetchingCode: PropTypes.bool.isRequired,
  fetchCodeTimer: PropTypes.number.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onReturnClick: PropTypes.func.isRequired,
  onFetchCodeClick: PropTypes.func.isRequired,
  form: PropTypes.objectOf(PropTypes.func).isRequired
}

export default Form.create()(ValidatePhone)
