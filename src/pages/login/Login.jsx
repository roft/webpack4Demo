/**
 * Created by linyi on 2019/3/11
 */
import React from 'react'
import { SceneWrapper } from './containers'
import './Login.scss'

function Login() {
  return (
    <section className="page--login">
      <div className="float-area">
        <div className="bg-box" />
        <SceneWrapper />
      </div>
    </section>
  )
}

Login.propTypes = {}

export default Login
