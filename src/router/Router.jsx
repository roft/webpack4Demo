import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import Loadable from 'react-loadable'
import Layout from '@/layout'

const loading = () => <p>loading...</p>
const render = (loaded, props) => {
  const { default: Component, title } = loaded
  document.title = title || '永辉中台系统'
  return <Component {...props} />
}

const Home = Loadable({
  loader: () => import('../pages/home'),
  loading,
  render
})
const Detail = Loadable({
  loader: () => import('@/pages/detail'),
  loading,
  render
})
const Login = Loadable({
  loader: () => import('@/pages/login'),
  loading,
  render
})
const OrderFund = Loadable({
  loader: () => import('@/pages/refund'),
  loading,
  render
})
const OrderFundDetail = Loadable({
  loader: () => import('@/pages/orderFundDetail'),
  loading,
  render
})
const NoMatch = () => <Redirect to="/orders" />

const Router = () => {
  const authedRoutes = (
    <Layout>
      <Switch>
        <Route path="/refunds" exact component={OrderFund} />
        <Route path="/refunds/:id/:isPartial" component={OrderFundDetail} />
        <Route component={NoMatch} />
      </Switch>
    </Layout>
  )

  return (
    <BrowserRouter>
      <Switch>
        {/* <Route exact path="/" component={Home} />
        <Route path="/detail" component={Detail} /> */}
        <Route path="/login" exact component={Login} />
        <Route path="" render={() => authedRoutes} />
      </Switch>
    </BrowserRouter>
  )
}

export default Router
