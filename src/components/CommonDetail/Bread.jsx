import React from 'react'
import { Breadcrumb } from 'antd'
import './style/Bread.scss'

const Bread = ({ route }) => (
  <div className="bread">
    <div className="iconStyle" />
    <Breadcrumb className="breadcrumb">
      {route
        && route.map(item => <Breadcrumb.Item key={item}>{item}</Breadcrumb.Item>)}
    </Breadcrumb>
  </div>
)
export default Bread
