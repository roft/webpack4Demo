/* eslint-disable no-shadow */
/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react'
import { LongText } from 'Comps'
import {
  Form, Table, Card, Row, Modal, Col,
} from 'antd'
import Bread from './Bread'
import './style/Detail.scss'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
}

class App extends Component {
  constructor() {
    super()
    this.state = {
      previewVisible: false,
      previewImage: null,
    }
  }

  handlePreview = url => {
    this.setState({
      previewImage: url,
      previewVisible: true,
    })
  }

  handleCancel = () => this.setState({ previewVisible: false })

  render() {
    const {
      form: { getFieldDecorator },
      detailData,
      detailConfig: { formItems },
      header,
    } = this.props
    const { previewImage, previewVisible } = this.state

    const head = header || (
      <Row className="head">
        <div className="title">{detailData.title}</div>
        <div className="extra">
          <div className="status">{detailData.status}</div>
          <div className="updateTime">{detailData.updateTime}</div>
        </div>
      </Row>
    )

    const body = formItems && formItems.length
      ? formItems.map((data, index) => (
        <Card
          title={<Bread route={[data.title]} />}
          className="noBottomLine"
          key={`${data}-${index}`}
        >
          <Form layout="inline">
            <Row>
              {data.list.map((item, index) => (
                <Col
                  span={
                        (item.type === 'item' && 8)
                        || (item.type === 'table' && 24)
                        || 24
                      }
                  className={
                        item.type === 'item' && (index + 1) % 3 !== 0
                          ? 'dash-line'
                          : ''
                      }
                  key={`${item}-${index}`}
                >
                  {item.type === 'item' && (
                    <Form.Item label={item.label}>
                      {getFieldDecorator(item.decorator, formItemLayout)(
                        <LongText title={detailData[item.decorator] || ''}>
                          <span className="long-text">{detailData[item.decorator]}</span>
                        </LongText>,

                      )}
                    </Form.Item>
                  )}
                  {item.type === 'image' && (
                    <Form.Item label={item.label}>
                      {getFieldDecorator(item.decorator)(
                        <Row>
                          {Array.isArray(detailData[item.decorator])
                                && detailData[item.decorator].map((src, index) => (
                                  <img
                                    role="button"
                                    onKeyPress={this.handlePreview.bind(
                                      this,
                                      detailData[item.decorator],
                                    )}
                                    style={{
                                      width: 100,
                                      height: 100,
                                      marginLeft: 10,
                                    }}
                                    alt="example"
                                    download="image"
                                    src={src}
                                    key={`${data}-${index}`}
                                  />
                                ))}
                        </Row>,
                      )}
                    </Form.Item>
                  )}
                  {item.type === 'table' && (
                    <Table
                      columns={item.columns}
                      dataSource={detailData[item.decorator]}
                      rowSelection={
                            item.rowSelection ? item.rowSelection : null
                          }
                    />
                  )}
                </Col>
              ))}
            </Row>
          </Form>
        </Card>
      ))
      : null

    const modal = (
      <Modal
        visible={previewVisible}
        footer={null}
        onCancel={this.handleCancel}
      >
        <img alt="example" style={{ width: '100%' }} src={previewImage} />
      </Modal>
    )

    return (
      <section className="orderFundDetail_detail">
        {head}
        {body}
        {modal}
      </section>
    )
  }
}

export default Form.create()(App)
