/**
 * Created by linyi on 2019/3/6
 */
import ShopSelector from './ShopSelector'
import SelectorWithAll from './SelectorWithAll'
import CommonDetail from './CommonDetail'
import Detail from './Detail'
import LongText from './LongText'

export { ShopSelector, SelectorWithAll, CommonDetail, Detail, LongText }

export default {
  ShopSelector,
  SelectorWithAll,
  CommonDetail,
  Detail,
  LongText
}
