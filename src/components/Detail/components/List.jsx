/**
 * Created by linyi on 2019/3/14
 */
import React from 'react'
import PropTypes from 'prop-types'
// import ReactOverflowTooltip from 'react-overflow-tooltip'
import { LongText } from 'Comps'
import './List.scss'

function List({ data }) {
  return (
    <ul className="comp__detail--list content-item-content">
      {data.map(([label, value]) => (
        <li key={label}>
          <span className="item-label">{`${label}：`}</span>
          <LongText title={value || ''}>
            <span className="item-value">{value}</span>
          </LongText>
        </li>
      ))}
    </ul>
  )
}

List.propTypes = {
  data: PropTypes.arrayOf(PropTypes.array).isRequired
}

export default List
