/**
 * Created by linyi on 2019/3/14
 */
import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'antd'
import { List } from './components'
import './Detail.scss'

const CompMapper = {
  list: List,
  table: Table
}

function Detail({ header, headerExtra, content }) {
  const contentChildren = content.map(({ title, component, compProps }) => {
    const Comp = CompMapper[component]
    return (
      <div className="content-item" key={title}>
        <h2>{title}</h2>
        <Comp {...compProps} />
      </div>
    )
  })

  return (
    <div className="comp__detail">
      <div className="header">
        <h1>{header}</h1>
        <div className="extra">{headerExtra}</div>
      </div>
      <div className="content">{contentChildren}</div>
    </div>
  )
}

Detail.propTypes = {
  header: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  headerExtra: PropTypes.node.isRequired,
  content: PropTypes.arrayOf(PropTypes.any).isRequired
}

export default Detail
