// import _ from "lodash";
import "./style.css";
import "./style.less";
// import "./style.scss";
import Icon from "./icon.png";
// import printMe from './print'
import { cube } from "./math.js";
// import { a } from './1.js'
import moment from "moment";
import { Button } from "antd";

if (process.env.NODE_ENV !== "production") {
  console.log("Looks like we are in development mode!");
}
// ;<Button type="primary">用户列表页</Button>

// console.log(a)
console.log(window);
async function f() {
  return await 123;
}
f().then(v => console.log(v));
Promise.resolve("hello_Promise").then(console.info);

console.log(Array.from(new Set([1, 2, 3])));
// console.log(Symbol('symbol'))

function* helloGenerator() {
  console.log("this is generator");
}
var h = helloGenerator();
h.next();

// console.log(Object.fromEntries([['a', '1'], ['b', '2']]))

const log = (target, name, descriptor) => {
  // 保存旧的方法add
  const oldValue = descriptor.initializer;

  descriptor.initializer = function() {
    // 输出日志
    console.log(`Calling ${name} with`, arguments);
    return oldValue.apply(this, arguments);
  };

  // 必须返回descriptor对象
  return descriptor;
};
class MyClass {
  @log
  add = (a, b) => {
    return a + b;
  };
}
new MyClass().add(1, 2);

class C {
  m() {}
}

async function getComponent() {
  var element = document.createElement("div");
  const _ = await import(/* webpackChunkName: "lodash" */ "lodash");
  element.innerHTML = _.join(["Hello", "webpack"], " ");
  return element;
}
getComponent().then(component => {
  document.body.appendChild(component);
});

function component() {
  var element = document.createElement("div");
  element.innerHTML = ["Hello webpack!", "5 cubed is equal to " + cube(5)].join(
    "\n\n"
  );
  //
  // lodash 是由当前 script 脚本 import 导入进来的
  // element.innerHTML = _.join(["Hello", "webpack"], " ");
  element.classList.add("hello");

  // 将图像添加到我们现有的 div。
  var myIcon = new Image();
  myIcon.src = Icon;

  element.appendChild(myIcon);

  var button = document.createElement("button");
  button.innerHTML = "lazyload";
  button.onclick = e =>
    import(/* /: "print" */ "./print").then(module => {
      var print = module.default;

      print();
    });
  element.appendChild(button);

  return element;
}

document.body.appendChild(component());

const less = () => {
  const element = document.createElement("div");
  element.setAttribute("id", "less");
  const str = "less编译成功的话背景会变成红色！";
  element.innerHTML = str;
  return element;
};
document.body.appendChild(less());

const sass = () => {
  const element = document.createElement("div");
  element.setAttribute("id", "sass");
  const str = "sass编译成功的话背景会变成蓝色！";
  element.innerHTML = str;
  return element;
};
document.body.appendChild(sass());

if (module.hot) {
  module.hot.accept("./print.js", function() {
    console.log("Accepting the updated printMe module!");
    printMe();
  });
}
