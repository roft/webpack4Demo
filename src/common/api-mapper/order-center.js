/*
 * @Author: linyi
 * @Date: 2019-03-12 10:54:28
 * @Last Modified by: linyi
 * @Last Modified time: 2019-03-13 23:22:19
 */

module.exports = {
  API_POST_REFUND_ORDERS_LIST: {
    // 退货订单列表
    host: 'adapter-mt',
    schema: '/api/refund/orders',
  },
  API_GET_REFUND_FULL_ORDER: {
    // 退款订单详情(全退)
    host: 'adapter-mt',
    schema: '/api/refund/full/order',
  },
  API_GET_REFUND_PART_ORDER: {
    // 退款订单详情(部分)
    host: 'adapter-mt',
    schema: '/api/refund/part/order',
  },
}
