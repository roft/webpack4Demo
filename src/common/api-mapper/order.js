/**
 * Created by linyi on 2019/3/12
 */
module.exports = {
  ORDER_STATUS: {
    // 订单状态
    host: 'adapter-mt',
    schema: '/common/order_status'
  },
  ORDER_LIST: {
    // 订单列表
    host: 'adapter-mt',
    schema: '/common/order/list'
  },
  ORDER_DETAIL: {
    // 订单详情
    host: 'adapter-mt',
    schema: '/common/order/details'
  }
}
