/**
 * Created by linyi on 2019/3/11
 */
const userCenter = require('./user-center')
const order = require('./order')
const orderCenter = require('./order-center')

module.exports = {
  userCenter,
  order,
  orderCenter
}
