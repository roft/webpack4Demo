/**
 * Created by linyi on 2019/3/11
 */
module.exports = {
  SIGN_IN: {
    // 登录
    host: 'user-center',
    schema: '/usercenter/login'
  },
  RESET_PWD_SMS_CODE: {
    // 发送忘记密码短信验证码
    host: 'user-center',
    schema: '/usercenter/sms_code'
  },
  CHECK_SMS_CODE: {
    // 校验短信验证码
    host: 'user-center',
    schema: '/usercenter/users/sms_code/check'
  },
  RESET_PASSWORD: {
    // 重置密码
    host: 'user-center',
    schema: '/usercenter/users/get_back'
  },
  SIGN_OUT: {
    // 退出登录
    host: 'user-center',
    schema: '/usercenter/logout'
  },
  MENUS: {
    // 菜单
    host: 'user-center',
    schema: '/usercenter/operation_objectives'
  }
}
