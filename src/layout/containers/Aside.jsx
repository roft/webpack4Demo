/**
 * Created by linyi on 2019/3/7
 */
import React from 'react'
import maxBy from 'lodash/maxBy'
import { request } from 'Utils'
import { userCenter as UserCenterApi } from 'ApiMapper'
import { withRouter } from 'react-router'
import AsideComp from '../components/Aside'

const { MENUS } = UserCenterApi

const findMenu = (arr, compared, comparedKey, childrenKey) => {
  const retrieve = (path = [], node) => {
    if (!node[childrenKey]) {
      const comparedValInNode = node[comparedKey]
      const nodeVal = comparedValInNode.startsWith('/')
        ? comparedValInNode.substr(1)
        : comparedValInNode
      if (!nodeVal) return { path: [...path, node], weight: 0 }
      if (nodeVal === compared)
        return { path: [...path, node], weight: Infinity }
      if (compared.startsWith(nodeVal))
        return { path: [...path, node], weight: 10 }
      return { path: [...path, node], weight: 0 }
    }

    const arrCompareResult = node[childrenKey].map(ele =>
      retrieve([...path, node], ele, compared)
    )

    return arrCompareResult.reduce(
      (acc, cur) => (cur.weight > acc.weight ? cur : acc),
      { path: [], weight: 0 }
    )
  }

  const tmp = arr.map(ele => retrieve([], ele))

  console.log(tmp)
  const result = maxBy(tmp, 'weight')
  return result && result.weight > 0 ? result.path : []
}

class Aside extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      menus: []
    }
  }

  componentDidMount() {
    this.setState({ loading: true })
    request(MENUS, 'GET', { params: { businessType: 41 } })
      .then(({ result }) => {
        this.setState({ menus: result })
      })
      .finally(() => this.setState({ loading: false }))
  }

  render() {
    const { menus, loading } = this.state
    const {
      location: { pathname }
    } = this.props

    const currentMenuPath = findMenu(
      menus,
      pathname.substr(1),
      'displayUrl',
      'operationObjectives'
    )
    const openKeys = currentMenuPath.map(ele => ele.id.toString())
    const selectedKeys = [openKeys.pop()]

    return (
      <AsideComp
        loading={loading}
        menus={menus}
        selectedKeys={selectedKeys}
        openKeys={openKeys}
      />
    )
  }
}

Aside.propTypes = {}

export default withRouter(Aside)
