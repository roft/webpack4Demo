/**
 * Created by linyi on 2019/3/7
 */
import { connect } from 'react-redux'
import BreadcrumbComp from '../components/Breadcrumb'

export default connect(
  state => ({
    data: state.layout.breadcrumb
  }),
  {}
)(BreadcrumbComp)
