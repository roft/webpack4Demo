/**
 * Created by linyi on 2019/3/7
 */
import React from 'react'
import { withRouter } from 'react-router'
import { request } from 'Utils'
import { userCenter } from 'ApiMapper'
import HeaderComp from '../components/Header'

const { SIGN_OUT } = userCenter

class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      username: ''
    }
  }

  componentDidMount() {
    const account = JSON.parse(localStorage.getItem('account')) || {
      name: '未登录'
    }
    this.setState({
      username: account.name
    })
  }

  onLogout = () => {
    const { history } = this.props
    request(SIGN_OUT, 'POST').finally(() => {
      localStorage.clear()
      history.push('/login')
    })
  }

  render() {
    return <HeaderComp {...this.state} onLogout={this.onLogout} />
  }
}

Header.propTypes = {}

export default withRouter(Header)
