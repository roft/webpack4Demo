/**
 * Created by linyi on 2019/3/6
 */
import { actions, reducer } from './module'
import Layout from './Layout'

export { actions, reducer }

export default Layout
