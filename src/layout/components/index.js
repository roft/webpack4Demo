/**
 * Created by linyi on 2019/3/7
 */
import Aside from './Aside'
import Breadcrumb from './Breadcrumb'
import Header from './Header'

export { Aside, Breadcrumb, Header }

export default {
  Aside,
  Breadcrumb,
  Header
}
