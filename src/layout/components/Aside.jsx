/**
 * Created by linyi on 2019/3/6
 */
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Menu, Icon } from 'antd'
import './Aside.scss'

const { SubMenu, Item: MenuItem } = Menu

const formatSubmenuTitle = obj => (
  <span>
    <Icon type={obj.code} />
    {obj.name}
  </span>
)

const mapMenuNode = menus =>
  menus.map(ele =>
    ele.operationObjectives ? (
      <SubMenu key={ele.id} title={formatSubmenuTitle(ele)}>
        {mapMenuNode(ele.operationObjectives)}
      </SubMenu>
    ) : (
      <MenuItem key={ele.id}>
        <Link to={ele.displayUrl || ''}>{ele.name}</Link>
      </MenuItem>
    )
  )

function Aside({ menus, openKeys, selectedKeys }) {
  return (
    <aside className="layout--aside">
      <div className="logo-text">永辉中台系统</div>
      <Menu
        mode="inline"
        selectedKeys={selectedKeys}
        openKeys={openKeys}
        onClick={obj => console.log(obj)}
      >
        {mapMenuNode(menus)}
      </Menu>
    </aside>
  )
}

Aside.propTypes = {
  menus: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedKeys: PropTypes.arrayOf(PropTypes.string).isRequired,
  openKeys: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default Aside
