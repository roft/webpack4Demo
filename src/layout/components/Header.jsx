/**
 * Created by linyi on 2019/3/6
 */
import React from 'react'
import PropTypes from 'prop-types'
import './Header.scss'
import UserPortrait from 'Imgs/user-portrait.png'

function Header({ username, onLogout }) {
  return (
    <header className="layout--header">
      <div className="user-info-box">
        <img src={UserPortrait} alt="user portrait" className="user-portrait" />
        <span>{username}</span>
      </div>
      <button
        type="button"
        className="btn-text button-primary"
        onClick={onLogout}
      >
        退出系统
      </button>
    </header>
  )
}

Header.propTypes = {
  username: PropTypes.string.isRequired,
  onLogout: PropTypes.func.isRequired
}

export default Header
