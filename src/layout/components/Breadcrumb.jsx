/**
 * Created by linyi on 2019/3/7
 */
import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb as AntBread } from 'antd'
import './Breadcrumb.scss'

const { Item } = AntBread

function Breadcrumb({ data }) {
  return (
    <AntBread className="layout--breadcrumb">
      {data.map(ele => (
        <Item key={ele}>{ele}</Item>
      ))}
    </AntBread>
  )
}

Breadcrumb.propTypes = {
  data: PropTypes.arrayOf(PropTypes.string)
}

Breadcrumb.defaultProps = {
  data: []
}

export default Breadcrumb
