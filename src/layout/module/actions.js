/**
 * Created by linyi on 2019/3/6
 */
import * as AT from './consts'

export const getMenus = () => ({
  type: AT.GET_MENUS
})

export const changeBreadcrumb = payload => ({
  type: AT.BREADCRUMB_CHANAGE,
  payload
})

export const appendBreadcrumb = payload => ({
  type: AT.BREADCRUMB_APPEND,
  payload
})

export default {
  getMenus,
  changeBreadcrumb,
  appendBreadcrumb
}
