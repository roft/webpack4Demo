/**
 * Created by linyi on 2019/3/6
 */
export const MENUS_REQUEST = 'layout/menu-request'
export const MENUS_SUCCESS = 'layout/menu-request-success'
export const MENUS_FAILURE = 'layout/menu-request-failure'

export const BREADCRUMB_CHANAGE = 'layout/breadcrumb-change'
export const BREADCRUMB_APPEND = 'layout/breadcrumb-append'

export const GET_MENUS = 'layout/breadcrumb-append'
