/**
 * Created by linyi on 2019/3/6
 */
import * as actions from './actions'
import reducer from './reducers'

export { actions, reducer }

export default {
  actions,
  reducer
}
