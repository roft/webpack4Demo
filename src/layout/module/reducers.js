/**
 * Created by linyi on 2019/3/6
 */
import { combineReducers } from 'redux'
import * as AT from './consts'

const menus = (state = { loading: false, data: [], error: '' }, action) => {
  switch (action.type) {
    case AT.MENUS_REQUEST:
      return {
        loading: true,
        data: [],
        error: ''
      }
    default:
      return state
  }
}

const breadcrumb = (state = [], action) => {
  switch (action.type) {
    case AT.BREADCRUMB_CHANAGE:
      return action.payload
    case AT.BREADCRUMB_APPEND:
      return state.append(action.payload)
    default:
      return state
  }
}

export default combineReducers({
  menus,
  breadcrumb
})
