/**
 * Created by linyi on 2019/3/7
 */
import React from 'react'
import PropTypes from 'prop-types'

import { Aside, Header, Breadcrumb } from './containers'

import './Layout.scss'

function Layout(props) {
  const { children } = props
  return (
    <div className="layout">
      <Aside />
      <section className="layout--content">
        <Header />
        <div className="layout--main">
          <Breadcrumb />
          {children}
        </div>
      </section>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node
}

Layout.defaultProps = {
  children: 'Put your page component here.'
}

export default Layout
