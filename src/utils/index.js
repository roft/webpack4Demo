/**
 * Created by linyi on 2019/3/11
 */
import request, { ORIGINAL_RESP } from './api'

export { request, ORIGINAL_RESP }

export default request
