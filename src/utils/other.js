/* 获取网址参数 */
const getURL = name => {
  const reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`)
  const r = window.location.search.substr(1).match(reg)
  return r !== null ? r[2] : null
}

/* 获取全部url参数,并转换成json对象 */
const getUrlAllParams = (url = window.location.href) => {
  const queryStr = url.substring(url.indexOf('?') + 1)
  const queryPairs = queryStr.split('&')

  const paramObj = {}
  queryPairs.forEach(ele => {
    const [key, val] = ele.split('=')
    paramObj[key] = window.decodeURIComponent(val)
  })

  return paramObj
}

/* 删除url指定参数，返回url */
function delParamsUrl(url, name) {
  const [baseUrl, queryStr] = url.split('?')

  if (!queryStr) return url

  const pairs = queryStr.split('&')
  const reg = new RegExp(`${name}(=.*)?`)
  const finalQs = pairs.filter(ele => !reg.test(ele)).join('&')
  return `${baseUrl}?${finalQs}`
}

/* eslint-disable no-bitwise */
/* 获取十六进制随机颜色 */
function getRandomColor() {
  return (Math.random() * 0x1000000 << 0).toString(16).padStart(7, '0')
}
/* eslint-enable */

/* 图片加载 */
const imgLoadAll = (arr, callback) => {
  const arrImg = []
  arr.forEach(imgSrc => {
    const img = new Image()
    img.src = imgSrc
    img.onload = function () {
      arrImg.push(this)
      if (arrImg.length === arr.length) {
        if (callback) callback()
      }
    }
  })
}

/* 音频加载 */
const loadAudio = (src, callback) => {
  const audio = new Audio(src)
  audio.onloadedmetadata = callback
  audio.src = src
}

/* DOM转字符串 */
const domToStirng = htmlDOM => {
  const div = document.createElement('div')
  div.appendChild(htmlDOM)
  return div.innerHTML
}

/* 字符串转DOM */
const stringToDom = htmlString => {
  const div = document.createElement('div')
  div.innerHTML = htmlString
  return div.children[0]
}

/**
 * 光标所在位置插入字符，并设置光标位置
 *
 * @param {dom} 输入框
 * @param {val} 插入的值
 * @param {posLen} 光标位置处在 插入的值的哪个位置
 */
const setCursorPosition = (dom, val, posLen) => {
  let cursorPosition = 0
  if (dom.selectionStart) {
    cursorPosition = dom.selectionStart
  }
  this.insertAtCursor(dom, val)
  dom.focus()
  console.log(posLen)
  dom.setSelectionRange(dom.value.length, cursorPosition + (posLen || val.length))
}

/* eslint-disable no-param-reassign */
/* 光标所在位置插入字符 */
const insertAtCursor = (dom, val) => {
  if (document.selection) {
    dom.focus()
    const sel = document.selection.createRange()
    sel.text = val
    sel.select()
  } else if (dom.selectionStart || dom.selectionStart === '0') {
    const startPos = dom.selectionStart
    const endPos = dom.selectionEnd
    const restoreTop = dom.scrollTop
    dom.value = dom.value.substring(0, startPos)
      + val + dom.value.substring(endPos, dom.value.length)
    if (restoreTop > 0) {
      dom.scrollTop = restoreTop
    }
    dom.focus()
    dom.selectionStart = startPos + val.length
    dom.selectionEnd = startPos + val.length
  } else {
    dom.value += val
    dom.focus()
  }
}
/* eslint-enable */

export default {
  getURL,
  getUrlAllParams,
  delParamsUrl,
  getRandomColor,
  imgLoadAll,
  loadAudio,
  domToStirng,
  stringToDom,
  setCursorPosition,
  insertAtCursor,
}
