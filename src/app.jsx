/*
 * @Author: linyi
 * @Date: 2019-05-09 17:04:53
 * @Last Modified by: linyi
 * @Last Modified time: 2020-06-08 09:24:02
 */

import React from 'react'
import { Provider } from 'react-redux'
import { LocaleProvider } from 'antd'
import zhCN from 'antd/lib/locale-provider/zh_CN'
import moment from 'moment'
import 'moment/locale/zh-cn'

import configureStore from './redux' 
import Router from './router'
// import 'Styles/base.scss'
// import 'Styles/common.scss'
// import 'Styles/reset-antd.scss'

moment.locale('zh-cn')

const store = configureStore()

const App = () => (
  <LocaleProvider locale={zhCN}>
    <Provider store={store}>
      <Router />
    </Provider>
  </LocaleProvider>
)

export default App
