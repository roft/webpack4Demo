/**
 * Created by linyi on 2019/3/5
 */
import { combineReducers } from 'redux'
import { reducer as layout } from '@/layout'

export default combineReducers({
  layout
})
