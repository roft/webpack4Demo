/**
 * Created by linyi on 2019/3/4
 */
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import apiMiddleware from './apiMiddleware'
import rootReducer from './rootReducer'

export default preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(applyMiddleware(thunk, apiMiddleware))
  )

  return store
}
