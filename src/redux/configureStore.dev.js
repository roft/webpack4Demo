/**
 * Created by linyi on 2019/3/4
 */
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import apiMiddleware from './apiMiddleware'
import rootReducer from './rootReducer'

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    composeEnhancers(applyMiddleware(thunk, apiMiddleware, createLogger()))
  )

  if (module.hot) {
    console.log('enable hot')
    module.hot.accept('./rootReducer.js', () => {
      console.log('--------')
      store.replaceReducer(rootReducer)
    })
  }

  return store
}
