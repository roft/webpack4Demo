/**
 * Created by linyi on 2019/3/4
 */
module.exports =
  process.env.NODE_ENV === 'production'
    ? require('./configureStore.prod')
    : require('./configureStore.dev')
