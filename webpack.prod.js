const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const Visualizer = require("webpack-visualizer-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

const NODE_ENV = process.env.NODE_ENV;

console.info(`当前环境：${NODE_ENV}`);

module.exports = merge(common, {
  devtool: "none", // 追求速度使用none, 放弃source map。整个 source map 作为一个单独的文件生成。它为 bundle 添加了一个引用注释，以便开发工具知道在哪里可以找到它
  // devtool: 'none', // 追求速度使用none, 放弃source map。整个 source map 作为一个单独的文件生成。它为 bundle 添加了一个引用注释，以便开发工具知道在哪里可以找到它
  mode: "production", // 设置当前模式为生产环境
  plugins: [
    // new UglifyJSPlugin({
    //   // 代码混淆
    //   sourceMap: true,
    //   uglifyOptions: {
    //     warnings: false,
    //     parse: {},
    //     compress: {
    //       warnings: false,
    //       drop_debugger: true,
    //       drop_console: true
    //     }
    //   }
    // }),下面optimization配置minimizer代替
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name].css",
      chunkFilename: "css/[contenthash:8].css"
    }),
    new OptimizeCssAssetsPlugin(), //压缩css插件
    // new Visualizer({ filename: 'statistics.html' }),//包分析
    // new BundleAnalyzerPlugin(), //包分析
    // new webpack.optimize.OccurrenceOrderPlugin(), 调整模块的打包顺序，用到次数更多的会出现在文件的前面 测试没效果。
    // new webpack.optimize.MinChunkSizePlugin({ minChunkSize: 10000 }), 通过合并小于 minChunkSize 大小的 chunk，将 chunk 体积保持在指定大小限制以上
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 5,
      minChunkSize: 1000
    }) //通过限制chunk的最大数和最小合并大小 来平衡http请求大小和chunk大小
  ],
  optimization: {
    // minimizer: [
    //   new UglifyJSPlugin({
    //     cache: true,
    //     parallel: true,
    //     sourceMap: true
    //   }),
    //   new TerserPlugin({ // 压缩js
    //     cache:  true,
    //     parallel:  true
    //   }
    // }),
    // ],指定mode: 'production'可以不用配置混淆压缩js
    runtimeChunk: {
      name: "runtime" //从bundle里的runtime提出来，因为就业务代码常常变化，这样可以利用长缓存缓存住mainfest和vender
    },
    splitChunks: {
      chunks: "async",
      name: true,
      cacheGroups: {
        vendors: {
          //这个是把第三方库提出来
          test: /node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        },
        common: {
          //这个是多入口情况下打出公共依赖的模块（单入口无效的） minChunks：2意味着至少被2个模块引用才提出来
          name: "common",
          chunks: "initial",
          minChunks: 2
        },
        styles: {
          name: "styles",
          test: /\.css$/,
          chunks: "all",
          enforce: true
        }
      }
    }
  }
});
