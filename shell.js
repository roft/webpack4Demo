const shell = require('shelljs')

if (!shell.which('git')) {
  shell.echo('需要git环境')
  shell.exit(1)
}

shell.exec('git add .')
shell.exec('git commit -m "auto commit"')
shell.exec('git push')

shell.echo('end')