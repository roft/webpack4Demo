const merge = require('webpack-merge')
const webpack = require('webpack')
const common = require('./webpack.common.js')
const path = require('path')

module.exports = merge(common, {
  devtool: 'eval', // 报错时方便调试
  mode: 'development', // 设置当前模式为开发模式
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    open: false, //不自动打开
    hot: true, // 启用 webpack 的模块热替换特性
    inline: true, // 在 dev-server 的两种不同模式之间切换。默认情况下，应用程序启用内联模式(inline mode)。这意味着一段处理实时重载的脚本被插入到你的包(bundle)中，并且构建消息将会出现在浏览器控制台
    host: 'localhost',
    port: 8081, //端口
    overlay: {
      // 展示编译的错误和警告
      warnings: true,
      errors: true
    },
    historyApiFallback: true //解决除了首页ok其他bad问题
    // colors: false,
    // modules: false,
    // children: false,
    // chunks: false,
    // chunkModules: false
  },
  plugins: [
    new webpack.NamedModulesPlugin(), // 当开启 HMR 的时候使用该插件会显示模块的相对路径
    new webpack.HotModuleReplacementPlugin(), // 当开启 HMR 的时候使用该插件会显示模块的相对路径
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    })
  ]
})
