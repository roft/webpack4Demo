module.exports = {
  host: 'localhost', // 本地服务ip
  port: 8080, // 端口
  historyApiFallback: true, // 不跳转
  inline: true, // 实时刷新
  open: false, // 是否自动打开网页
}
